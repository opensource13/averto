package main

import (
	_ "embed"
	"log"

	"github.com/everitosan/averto/cmd/api/bootstrap"
)

func main() {
	logFile, err := bootstrap.Run()
	if err != nil {
		log.Fatal(err)
	}

	if logFile != nil {
		defer logFile.Close()
	}
}
