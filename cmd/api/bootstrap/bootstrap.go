package bootstrap

import (
	_ "embed"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/everitosan/averto/internal/server"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
)

var (
	Version string = strings.TrimSpace(version)
	//go:embed version.txt
	version string
)

func Run() (*os.File, error) {

	err := godotenv.Load()

	logrus.Infof("Averto server (version %s)", version)

	if err != nil {
		logrus.Infof("No .env file detected %v", err)
	}

	port, err := strconv.Atoi(os.Getenv("AVERTO_PORT"))
	if err != nil {
		message := fmt.Sprintf("bad config port %v", err)
		return nil, errors.New(message)
	}

	config := server.Config{
		Port:     int16(port),
		HttpAddr: os.Getenv("AVERTO_HOST"),
	}

	srv := server.New(&config)

	f := SetLogger()
	return f, srv.Run()
}
