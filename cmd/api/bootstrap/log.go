package bootstrap

import (
	"log"
	"os"

	"github.com/sirupsen/logrus"
)

func SetLogger() *os.File {
	debug := os.Getenv("AVERTO_DEBUG")
	debugFile := os.Getenv("AVERTO_LOG_FILE")
	logrus.Debugln("Setting log")

	if debug == "true" {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if debugFile != "" {
		f, err := os.OpenFile(debugFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			log.Println("Failed to create logfile" + debugFile)
			panic(err)
		}
		logrus.SetOutput(f)
		logrus.Debugf("Using file %s", debugFile)
		return f
	} else {
		return nil
	}
}
