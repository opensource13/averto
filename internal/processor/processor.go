package processor

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/everitosan/averto/internal/validator"
	"github.com/sirupsen/logrus"
)

var id int64

func getTaskId() int64 {
	newId := id + 1
	id = newId
	return id
}

func RunAction(a chan validator.Action) {

	var stdBuffer bytes.Buffer
	mw := io.MultiWriter(os.Stdout, &stdBuffer)

	task := <-a
	taskId := getTaskId()

	logrus.Infof("Starting task %d %s", taskId, task.Name)

	command := filepath.Join(os.Getenv("AVERTO_WORKDIR"), task.Execute)
	cmd := exec.Command(command)
	cmd.Stdout = mw
	cmd.Stderr = mw

	err := cmd.Run()

	if err != nil {
		logrus.Errorf("Error running %d %s: %s", taskId, task.Name, err)
	} else {
		logrus.Infof("Finished task %d %s", taskId, task.Name)
		logrus.Debugf("Output of task %d: %s", taskId, stdBuffer.String())
	}
}
