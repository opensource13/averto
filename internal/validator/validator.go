package validator

import (
	"fmt"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v3"

	"github.com/Jeffail/gabs"
	"github.com/sirupsen/logrus"
)

type Action struct {
	Name        string
	Description string
	Execute     string
	Token       string
	Conditions  map[interface{}]interface{}
}

type Validator struct {
	actions map[string][]Action
}

func filterYamlFiles(workdir string) ([]string, error) {
	var files []string

	_, err := os.Stat(workdir)
	if err != nil {
		return files, err
	}

	err = filepath.Walk(workdir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			if filepath.Ext(path) == ".yaml" || filepath.Ext(path) == ".yml" {
				files = append(files, path)
			}
		}
		return nil
	})
	return files, err
}

func NewValidator(workdir string) (*Validator, error) {
	var actions = make(map[string][]Action)

	// Read inside workdir
	files, err := filterYamlFiles(workdir)
	if err != nil {
		return &Validator{}, fmt.Errorf("%w: %s", ErrValidatorReadWorkDir, err)
	}

	for _, path := range files {
		// Read the required file
		logrus.Debugf("Working with %s", path)
		file, err := os.ReadFile(path)
		if err != nil {
			return &Validator{}, fmt.Errorf("%w: %s", ErrValidatorReadSource, err)
		}
		// Parse the actions into an array
		var data []Action
		err1 := yaml.Unmarshal(file, &data)
		if err1 != nil {
			return &Validator{}, fmt.Errorf("%w: %s", ErrValidatorParseSource, err)
		}

		// Separate actions into token key map
		for _, action := range data {
			logrus.Debugf("Action [%s: %s]", action.Token, action.Name)
			tokenAction, exist := actions[action.Token]
			if !exist {
				actions[action.Token] = []Action{action}
			} else {
				actions[action.Token] = append(tokenAction, action)
			}
		}
	}
	// Creates validator
	v := Validator{
		actions,
	}

	logrus.Infof("%d actions detected inside workdir '%s'", len(actions), workdir)

	return &v, nil
}

func (v *Validator) Check(token string, body []byte) (Action, error) {
	var finalAction Action

	tokenActions, exists := v.actions[token]
	if !exists {
		return Action{}, ErrValidatorMissingAction
	}

	jsonParsed, err := gabs.ParseJSON(body)

	if err != nil {
		return Action{}, fmt.Errorf("%w: %s", ErrValidatorParseBody, err)
	}

	for _, action := range tokenActions {

		not_complying := false

		for key, value := range action.Conditions {
			actualKey := fmt.Sprintf("%s", key)
			received := jsonParsed.Path(actualKey).Data().(string)
			if value != received {
				not_complying = true
				break
			}
		}

		if !not_complying { // No error ocurred in previuos check of condition set
			finalAction = action
			break
		}
	}

	if finalAction.Execute != "" { // We have an execution found
		return finalAction, nil
	}

	return Action{}, ErrValidatorMissingAction
}
