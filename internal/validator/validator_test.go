package validator

import (
	"errors"
	"testing"
)

func TestReader(t *testing.T) {
	/*
	* This test reads an existing file
	 */
	_, err := NewValidator("../../examples/GitLab/")
	if err != nil {
		t.Errorf("Error creating validator: %v", err)
	} else {
		t.Log("Validator created")
	}
}

func TestReaderBadFile(t *testing.T) {
	/*
	* This test tries to read a missing directory and checks validator returns correct process.Error
	 */
	_, err := NewValidator("./missing")
	if err != nil {
		switch {
		case errors.Is(err, ErrValidatorReadWorkDir):
			t.Logf("Expected read error %v", err)
			return
		default:
			t.Errorf("Wrong error type ?")
			return
		}
	} else {
		t.Errorf("This should not happen, error mismatch")
	}
}

func TestReaderBadParseBody(t *testing.T) {
	/*
	* This test tries to parse a wrong formed body
	 */

	validator, err := NewValidator("../../examples/GitLab/push.yml")
	if err != nil {
		t.Errorf("Error creating validator: %v", err)
	}

	wrongBody := []byte("some random not json formated text")
	_, err = validator.Check("", wrongBody)

	if err != nil {
		switch {
		case errors.Is(err, ErrValidatorMissingAction):
			t.Log("Expected parse body error")
			return
		default:
			t.Errorf("Wrong error type ? %v", err)
			return
		}
	} else {
		t.Errorf("This should not happen, error mismatch")
	}

}
