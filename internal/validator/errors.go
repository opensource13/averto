package validator

import "errors"

var ErrValidatorReadWorkDir = errors.New("unable to use the workdir")
var ErrValidatorReadSource = errors.New("unable to read source yaml file")
var ErrValidatorParseSource = errors.New("unable to parse yaml file")
var ErrValidatorParseBody = errors.New("unable to parse the body payload")
var ErrValidatorMissingAction = errors.New("unable to find corresponding action")
