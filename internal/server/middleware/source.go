package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func SourceCheck(handlerFun gin.HandlerFunc) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ""

		// Gitlab case
		token = ctx.Request.Header.Get("X-Gitlab-Token")

		if token != "" {
			ctx.Set("token", token)
			handlerFun(ctx)
			return
		}

		ctx.AbortWithStatus(http.StatusForbidden)
	}
}
