package server

import (
	"fmt"
	"log"
	"os"

	"github.com/everitosan/averto/internal/server/middleware"
	"github.com/everitosan/averto/internal/validator"
	"github.com/gin-gonic/gin"
)

// Configuration for the server
type Config struct {
	Port     int16
	HttpAddr string
}

type Server struct {
	config    *Config
	validator *validator.Validator
	engine    *gin.Engine
}

func New(config *Config) Server {
	debug := os.Getenv("AVERTO_DEBUG")
	if debug != "true" {
		gin.SetMode(gin.ReleaseMode)
	}

	server := Server{
		config: config,
		engine: gin.New(),
	}

	server.addValidator()
	server.registerHandlers()

	return server
}

func (s *Server) registerHandlers() {
	s.engine.POST("/", middleware.SourceCheck(CheckMessage(s.validator)))
}

func (s *Server) addValidator() {
	path := os.Getenv("AVERTO_WORKDIR")
	validator, err := validator.NewValidator(path)
	if err != nil {
		log.Fatal(err)
	}
	s.validator = validator
}

func (s *Server) Run() error {
	address := fmt.Sprintf("%s:%d", s.config.HttpAddr, s.config.Port)
	return s.engine.Run(address)
}
