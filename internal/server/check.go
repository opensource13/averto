package server

import (
	"errors"
	"io"
	"net/http"

	"github.com/everitosan/averto/internal/processor"
	"github.com/everitosan/averto/internal/validator"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type okResponse struct {
	status string
}

func CheckMessage(v *validator.Validator) gin.HandlerFunc {
	return func(ctx *gin.Context) {

		body, err := io.ReadAll(ctx.Request.Body)

		if err != nil {
			logrus.Errorf("Bad body: %s", err)
			ctx.AbortWithError(http.StatusInternalServerError, errors.New("wrong body payload"))
			return
		}

		logrus.Debugf("%s", body)

		token := ctx.GetString("token")

		foundAction, err := v.Check(token, body)
		if err != nil {
			logrus.Errorln("No Action found")
			ctx.JSON(http.StatusOK, "")
			return
		}

		logrus.Infof("Action found: %s (%s)", foundAction.Name, foundAction.Description)
		a := make(chan validator.Action)
		go processor.RunAction(a)
		a <- foundAction

		ctx.JSON(http.StatusOK, okResponse{status: "ok"})
	}
}
