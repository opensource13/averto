# Averto

*A webserver for webhooks written in go.*

> Working only for GitLab

The idea behind this project is to avoid rewritting webhook receivers to handle notifications from gitlab or github.

## Index

- [👷‍♀️ Usage](#Usage)
- [🔍 Examples](#Examples)
- [⚙️ Variables](#Variables)
- [📦 Instalation](#Instalation)
- [🔖 References](#References)
- [🏗️ TODO](#TODO)

---

</br>


## Usage
</br>

When it receives a payload, it will check if the body matches with conditions written in the yml specification file.


This is a part of the json body that GitLab sends when a push webhook it's triggered.
```json
{
  "object_kind": "push",
  "event_name": "push",
  "before": "95790bf891e76fee5e1747ab589903a6a1f80f22",
  "after": "da1560886d4f094c3e6c9ef40349f7d",
  ...

  "project":{
    "id": 15,
    "name":"Diaspora",
    ...
  }
}
```

We can use that information to write our custom yml file.

```yml
- name: Code update
  description: Updates the code in every push
  execute: push.sh
  token: 12345
  conditions:
    project.name: Diaspora
    object_kind: push
    event_name: push

```

*When the server receives a request, it will check and select the first specification that matches the conditions, then it will answer the request and delegate via **go rutines** the desired execution.*


```mermaid
graph TD
  Webhook -- JSON payload --> Averto
  Averto -- Response Http --> Webhook

  subgraph "fa:fa-server Server"
    Averto --> Validate{Validate}
    Validate --> |accomplish| Executor[fa:fa-gear Executor]
    Validate --> |not accomplish| Log
    Executor --> Log
  end

  subgraph "fa:fa-code GitLab"
    Ci[CI/CD process] --> Webhook
  end
```

---
</br>

## Examples
</br>

To have a better understanding of the project there is a examples directory where you can find:
- [Insomnia](https://insomnia.rest/download) request example
- Definiton yml file
- Execution file

---
</br>

## Variables
</br>
List of env variables for the server.  

| Variable | Description |
|--|--|
|AVERTO_PORT| Port for the server to listen requests|
|AVERTO_HOST| Host of te server|
|AVERTO_WORKDIR | Place where it will search for specification files and executables|
|AVERTO_DEBUG| Indicates whether the server runs in debug mode or not, this changes produce aditional log|
|AVERTO_LOG_FILE|If present, it will try to save log to this file |

---
</br>

## Instalation
</br>

There are some packages under the [Releases page](https://gitlab.com/opensource13/averto/-/releases).



To generate a unit file for systemD and use it as a service you may want to check [this script](https://github.com/everitosan/BashScripts).

```bash
bash <(curl -s https://raw.githubusercontent.com/everitosan/BashScripts/main/systemd/install.sh)
```

## References
- [Gitlab Webhook events](https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html)
- [Github webhooks](https://docs.github.com/en/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#delivery-headers)

## Development
### Local
To run the app in local, you can use air to have auto compiling at every change in code.

```bash
# Install air tool
$ go install github.com/cosmtrek/air@latest
# Start Averto server
$ air
```
### Release
To generate a new release by the gitlab pipeline two conditions should accomplished.
- Push to main branch
- Push should contain tags

In order to have a better managment of this versions, there is a script that should be run only inside the main branch.

```bash
./utils/version.sh -a vx.x.x
```

### TODO
- [ ] Create logs per task
- [ ] Add Github support